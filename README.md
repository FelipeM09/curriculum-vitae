![Banner](imagenes/Portada-web.png)
# Titulo del Proyecto

#### Curriculum Vitae


## índice
1. [Características del Proyecto](#características)
2. [Contenido del Proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autor)
8. [Institución Académica](#institución-académica)




#### Características
- Uso de CSS recomendado: [ver](https://gitlab.com/FelipeM09/curriculum-vitae/-/tree/master/css)
#### Contenido del Proyecto
- [index.html](https://gitlab.com/FelipeM09/curriculum-vitae/-/blob/master/index.html) : Este archivo visualiza la página principal.
#### Tecnologías
- Uso de Bootstrap : [Ir](https://getbootstrap.com/) O si bien puede aprender Bootstrap [Aquí](https://www.youtube.com/watch?v=nug1pMke-y4)
#### IDE
- Sublime text [Cómo usarlo](https://www.youtube.com/watch?v=7mpjfkvdUBc)
#### Instalación

1. Local
    - Descargar el repositorio ubicado en: [Descargar](https://gitlab.com/FelipeM09/curriculum-vitae/)
    - Abrir el archivo index.html desde el navegador predeterminado

2. Gitlab
    - Realizando un fork.    
#### Demo
Se puede visualizar la versión del demo [Aquí.](https://felipem09.gitlab.io/curriculum-vitae/)
#### Autor(es)
Realizado por:[Manuel Felipe Mora](<manuelfelipeme@ufps.edu.co>)
#### Institución Académica

Proyecto realizado por el programa de [Ingeniería de Sistemas](https://ingsistemas.cloud.ufps.edu.co/) de la [Universidad Francisco de Paula Santander](https://ww2.ufps.edu.co/)


